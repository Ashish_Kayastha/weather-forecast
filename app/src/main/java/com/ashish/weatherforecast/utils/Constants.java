package com.ashish.weatherforecast.utils;

import com.ashish.weatherforecast.BuildConfig;
import com.ashish.weatherforecast.R;

import java.util.HashMap;
import java.util.Map;

public class Constants {

    public static final int PRIMARY_BLACK_COLOR = 0xFF212121;
    public static final String API_KEY = BuildConfig.DARK_SKY_API_KEY;
    public static final String DARK_SKY_API_BASE_URL = "https://api.darksky.net/forecast/"
            + API_KEY + "/";

    public static final Map<String, Integer> WEATHER_ICONS_MAP = new HashMap<String, Integer>() {
        {
            put("clear", R.string.wi_wu_clear);
            put("clear-day", R.string.wi_dark_sky_clear_day);
            put("clear-night", R.string.wi_dark_sky_clear_night);
            put("rain", R.string.wi_dark_sky_rain);
            put("snow", R.string.wi_dark_sky_snow);
            put("sleet", R.string.wi_dark_sky_sleet);
            put("wind", R.string.wi_dark_sky_wind);
            put("fog", R.string.wi_dark_sky_fog);
            put("cloudy", R.string.wi_dark_sky_cloudy);
            put("partly-cloudy-day", R.string.wi_dark_sky_partly_cloudy_day);
            put("partly-cloudy-night", R.string.wi_dark_sky_partly_cloudy_night);
            put("hail", R.string.wi_dark_sky_hail);
            put("thunderstorm", R.string.wi_dark_sky_thunderstorm);
            put("tornado", R.string.wi_dark_sky_tornado);
        }
    };

    public static final Map<String, Integer> CARD_COLOR_MAP = new HashMap<String, Integer>() {
        {
            put("clear", 0xFFF9A825);
            put("clear-day", 0xFFF9A825);
            put("clear-night", 0xFF455A64);
            put("rain", 0xFF448AFF);
            put("snow", 0xFF929394);
            put("sleet", 0xFFFCDF03);
            put("wind", 0xFFFCDF03);
            put("fog", 0xFF616161);
            put("cloudy", 0xFF90A4AE);
            put("partly-cloudy-day", 0xFF90A4AE);
            put("partly-cloudy-night", 0xFF90A4AE);
            put("hail", 0xFFFCDF03);
            put("thunderstorm", 0xFFFCDF03);
            put("tornado", 0xFFFCDF03);
        }
    };
}
