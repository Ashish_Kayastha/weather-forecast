package com.ashish.weatherforecast.utils;

import android.app.Activity;
import android.content.Intent;

import com.ashish.weatherforecast.R;
import com.ashish.weatherforecast.app.WeatherForecastApp;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;

import java.util.Arrays;
import java.util.List;

import static android.app.Activity.RESULT_OK;

public class AutoCompletePlaceHelper {

    private static final int RC_SELECT_PLACE = 1000;
    private static final int RC_PLAY_SERVICES_ERROR = 1001;

    private final Activity activity;

    public AutoCompletePlaceHelper(Activity activity) {
        this.activity = activity;

        if (!Places.isInitialized()) {
            Places.initialize(WeatherForecastApp.getAppContext(), activity
                    .getString(R.string.google_maps_api_key));
        }
    }

    public void startAutoCompletePlacesActivity() {
        final GoogleApiAvailability api = GoogleApiAvailability.getInstance();
        final int resultCode = api.isGooglePlayServicesAvailable(activity);

        if (resultCode == ConnectionResult.SUCCESS) {
            List<Place.Field> fields = Arrays.asList(Place.Field.NAME, Place.Field.LAT_LNG);
            Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.OVERLAY,
                    fields)
                    .build(activity);

            activity.startActivityForResult(intent, RC_SELECT_PLACE);
        } else {
            if (api.isUserResolvableError(resultCode)) {
                api.getErrorDialog(activity, resultCode, RC_PLAY_SERVICES_ERROR)
                        .show();
            }
        }
    }

    public Place getSelectedPlace(int requestCode, int resultCode, Intent data) {
        Place place = null;
        if (requestCode == RC_SELECT_PLACE && resultCode == RESULT_OK) {
            place = Autocomplete.getPlaceFromIntent(data);
        }

        return place;
    }
}