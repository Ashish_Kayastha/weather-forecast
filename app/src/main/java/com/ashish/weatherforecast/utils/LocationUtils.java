package com.ashish.weatherforecast.utils;

import android.location.Address;
import android.location.Geocoder;

import com.ashish.weatherforecast.app.WeatherForecastApp;
import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.List;

public final class LocationUtils {

    private LocationUtils() {
        throw new AssertionError("No instances");
    }

    public static Address getAddress(LatLng latLng) throws IOException {
        Address address = null;
        if (latLng != null) {
            Geocoder geocoder = new Geocoder(WeatherForecastApp.getAppContext());
            List<Address> addressList = geocoder
                    .getFromLocation(latLng.latitude, latLng.longitude, 1);
            if (addressList != null && addressList.size() > 0) {
                address = addressList.get(0);
            }
        }

        return address;
    }

    public static String getLatLng(LatLng latLng, String separator) {
        DecimalFormat df = new DecimalFormat("#.###");
        df.setRoundingMode(RoundingMode.CEILING);
        return df.format(latLng.latitude) + separator + df.format(latLng.longitude);
    }
}