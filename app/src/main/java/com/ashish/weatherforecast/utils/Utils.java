package com.ashish.weatherforecast.utils;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.TextView;

import com.ashish.weatherforecast.R;
import com.ashish.weatherforecast.app.WeatherForecastApp;
import com.google.android.material.snackbar.Snackbar;

import java.math.RoundingMode;
import java.text.DecimalFormat;

public class Utils {

    private Utils() {
        throw new AssertionError("No instances");
    }

    public static boolean isNullOrEmpty(String text) {
        return text == null || text.isEmpty();
    }

    public static void showSnackbar(Activity activity, int mainTextStringId, int actionStringId,
                                    View.OnClickListener listener) {
        Snackbar.make(activity.findViewById(R.id.coordinator_layout),
                activity.getString(mainTextStringId), Snackbar.LENGTH_INDEFINITE)
                .setAction(activity.getString(actionStringId), listener)
                .show();
    }

    public static boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) WeatherForecastApp.getAppContext()
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = cm != null ? cm.getActiveNetworkInfo() : null;
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static String getRoundFormattedText(String format, double value) {
        return String.format(format, Math.round(value));
    }

    public static String getWindDirection(int windBearing) {
        double value = Math.floor((windBearing / 22.5) + 0.5);
        String[] directions = {"N", "NNE", "NE", "ENE", "E", "ESE", "SE", "SSE", "S", "SSW", "SW",
                "WSW", "W", "WNW", "NW", "NNW"};
        return directions[(int) value % 16];
    }

    public static Animation rotateWindIcon(int windBearing) {
        RotateAnimation rotate = new RotateAnimation(0f, windBearing,
                Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        rotate.setFillAfter(true);
        return rotate;
    }

    public static void showOrHideText(TextView textView, String text) {
        if (Utils.isNullOrEmpty(text)) {
            textView.setVisibility(View.GONE);
        } else {
            textView.setVisibility(View.VISIBLE);
            textView.setText(text);
        }
    }

    public static String getTemperature(double temperature) {
        return getRoundFormattedText("%d°", temperature);
    }

    public static String formatTo3DecimalPlaces(double value) {
        DecimalFormat df = new DecimalFormat("#.###");
        df.setRoundingMode(RoundingMode.CEILING);
        df.setMinimumFractionDigits(1);
        return df.format(value);
    }
}