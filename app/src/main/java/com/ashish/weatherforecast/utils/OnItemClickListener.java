package com.ashish.weatherforecast.utils;

public interface OnItemClickListener {

    void onItemClick(int position);
}