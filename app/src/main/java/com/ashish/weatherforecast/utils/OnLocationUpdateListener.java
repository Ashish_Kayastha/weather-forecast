package com.ashish.weatherforecast.utils;

import com.google.android.gms.maps.model.LatLng;

public interface OnLocationUpdateListener {

    void onLocationUpdated(LatLng latLng);
}