package com.ashish.weatherforecast.utils;

import android.Manifest;
import android.app.Activity;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import com.ashish.weatherforecast.R;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;

public class LocationManager {

    private static final String TAG = "LocationManager";
    private static final int REQUEST_CHECK_SETTINGS = 0x1;
    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;

    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 30000;
    private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS =
            UPDATE_INTERVAL_IN_MILLISECONDS / 2;

    private static final String[] LOCATION_PERMISSIONS = {
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION
    };

    private Activity activity;
    private SettingsClient settingsClient;
    private LocationRequest locationRequest;
    private LocationCallback locationCallback;
    private LocationSettingsRequest locationSettingsRequest;
    private FusedLocationProviderClient fusedLocationClient;
    private OnLocationUpdateListener locationUpdateListener;

    private boolean requestLocationUpdates = true;
    private ArrayList<String> rejectedPermissionsList = new ArrayList<>();

    public LocationManager(@NonNull Activity activity, OnLocationUpdateListener listener) {
        this.activity = activity;
        locationUpdateListener = listener;

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(activity);
        settingsClient = LocationServices.getSettingsClient(activity);

        createLocationRequest();
        createLocationCallback();
        buildLocationSettingsRequest();
    }

    private void createLocationRequest() {
        locationRequest = new LocationRequest();
        locationRequest.setNumUpdates(1);
        locationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        locationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    private void createLocationCallback() {
        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                Location location = locationResult.getLastLocation();
                if (location != null) {
                    LatLng currentLatLng = new LatLng(location.getLatitude(),
                            location.getLongitude());
                    locationUpdateListener.onLocationUpdated(currentLatLng);
                    requestLocationUpdates = false;
                    stopLocationUpdates();
                }
            }
        };
    }

    private void buildLocationSettingsRequest() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(locationRequest);
        locationSettingsRequest = builder.build();
    }

    public void checkLocationUpdates() {
        if (requestLocationUpdates && isLocationPermissionGranted()) {
            startLocationUpdates();
        } else if (!isLocationPermissionGranted()) {
            requestPermissions();
        }
    }

    private void startLocationUpdates() {
        // Begin by checking if the device has the necessary location settings.
        settingsClient.checkLocationSettings(locationSettingsRequest)
                .addOnSuccessListener(activity, locationSettingsResponse -> {
                    //noinspection MissingPermission
                    fusedLocationClient.requestLocationUpdates(locationRequest,
                            locationCallback, Looper.myLooper());
                })
                .addOnFailureListener(activity, error -> {
                    int statusCode = ((ApiException) error).getStatusCode();
                    switch (statusCode) {
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            try {
                                // Show the dialog by calling startResolutionForResult(), and
                                // check the result in onActivityResult().
                                ResolvableApiException rae = (ResolvableApiException) error;
                                rae.startResolutionForResult(activity, REQUEST_CHECK_SETTINGS);
                            } catch (IntentSender.SendIntentException e) {
                                Log.e(TAG, e.getMessage());
                            }
                            break;

                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            String errorMessage = "Location settings are inadequate, "
                                    + "and cannot be fixed here. Please fix in Settings.";
                            Toast.makeText(activity, errorMessage, Toast.LENGTH_LONG)
                                    .show();
                            requestLocationUpdates = false;
                    }
                });
    }

    public void checkActivityResult(int requestCode, int resultCode) {
        if (requestCode == REQUEST_CHECK_SETTINGS && resultCode == Activity.RESULT_CANCELED) {
            requestLocationUpdates = false;
        }
    }

    public void stopLocationUpdates() {
        fusedLocationClient.removeLocationUpdates(locationCallback)
                .addOnCompleteListener(activity, task -> requestLocationUpdates = false);
    }

    private boolean isLocationPermissionGranted() {
        return hasPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                && hasPermission(Manifest.permission.ACCESS_COARSE_LOCATION);
    }

    private boolean hasPermission(String permission) {
        return ActivityCompat.checkSelfPermission(activity, permission)
                == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermissions() {
        boolean shouldShowRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(activity,
                        Manifest.permission.ACCESS_FINE_LOCATION)
                        && ActivityCompat.shouldShowRequestPermissionRationale(activity,
                        Manifest.permission.ACCESS_COARSE_LOCATION);

        // Show an additional rationale to the user. This would happen if the user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldShowRationale) {
            Utils.showSnackbar(activity, R.string.permission_rationale,
                    android.R.string.ok, view ->
                            ActivityCompat.requestPermissions(activity, LOCATION_PERMISSIONS,
                                    REQUEST_PERMISSIONS_REQUEST_CODE));
        } else {
            // Request permission. It's possible this can be auto answered if device policy
            // sets the permission in a given state or the user denied the permission
            // previously and checked "Never ask again".
            ActivityCompat.requestPermissions(activity, LOCATION_PERMISSIONS,
                    REQUEST_PERMISSIONS_REQUEST_CODE);
        }
    }

    public void onRequestPermissionsResult(int requestCode) {
        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            for (String permission : LOCATION_PERMISSIONS) {
                if (!hasPermission(permission)) {
                    rejectedPermissionsList.add(permission);
                }
            }

            if (rejectedPermissionsList.size() > 0) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(activity,
                        rejectedPermissionsList.get(0))) {
                    showRejectedPermissionDialog();
                }
            } else {
                if (requestLocationUpdates) {
                    startLocationUpdates();
                }
            }
        }
    }

    private void showRejectedPermissionDialog() {
        new AlertDialog.Builder(activity)
                .setMessage("These permissions are mandatory to get your "
                        + "location. You need to allow them.")
                .setPositiveButton(android.R.string.ok, (dialogInterface, i) ->
                        ActivityCompat.requestPermissions(activity, rejectedPermissionsList.toArray(
                                new String[rejectedPermissionsList.size()]),
                                REQUEST_PERMISSIONS_REQUEST_CODE))
                .setNegativeButton(android.R.string.cancel, null)
                .create()
                .show();
    }
}