package com.ashish.weatherforecast.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class DateTimeUtils {

    private DateTimeUtils() {
        throw new AssertionError("No instances");
    }

    public static String formatTime(String pattern, long time, TimeZone timeZone) {
        Date date = new Date(time * 1000);
        SimpleDateFormat sdf = new SimpleDateFormat(pattern, Locale.getDefault());
        sdf.setTimeZone(timeZone);
        return sdf.format(date);
    }

    public static String getTimeAndDay(long time) {
        return formatTime("HH:mm (EEE)", time, TimeZone.getDefault());
    }

    public static String convertTimeStamp(long time) {
        return convertTimeStamp(time, TimeZone.getDefault());
    }

    public static String convertTimeStamp(long time, TimeZone timeZone) {
        return formatTime("HH:mm", time, timeZone);
    }

    public static String getDayOfMonth(long time) {
        Calendar cal = Calendar.getInstance();
        int currentDay = cal.get(Calendar.DAY_OF_MONTH);

        cal.setTime(new Date(time * 1000));
        int dayOfMonth = cal.get(Calendar.DAY_OF_MONTH);
        String dayName = cal.getDisplayName(Calendar.DAY_OF_WEEK,
                Calendar.LONG, Locale.getDefault());

        return currentDay == dayOfMonth ? "Today" : dayName;
    }
}