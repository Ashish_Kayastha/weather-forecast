package com.ashish.weatherforecast.data.models;

import android.os.Parcel;
import android.os.Parcelable;

public class Alert implements Parcelable {

    public static final Creator<Alert> CREATOR = new Creator<Alert>() {
        @Override
        public Alert createFromParcel(Parcel in) {
            return new Alert(in);
        }

        @Override
        public Alert[] newArray(int size) {
            return new Alert[size];
        }
    };

    private int expires;
    private String description;
    private int time;
    private String title;
    private String uri;

    protected Alert(Parcel in) {
        expires = in.readInt();
        description = in.readString();
        time = in.readInt();
        title = in.readString();
        uri = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(expires);
        dest.writeString(description);
        dest.writeInt(time);
        dest.writeString(title);
        dest.writeString(uri);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public int getExpires() {
        return expires;
    }

    public String getDescription() {
        return description;
    }

    public int getTime() {
        return time;
    }

    public String getTitle() {
        return title;
    }

    public String getUri() {
        return uri;
    }
}
