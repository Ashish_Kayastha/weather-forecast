package com.ashish.weatherforecast.data.api;

import com.ashish.weatherforecast.data.models.WeatherForecast;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface WeatherService {

    @GET("{latlng}?units=si&exclude=minutely,flags")
    Single<WeatherForecast> getWeatherForecast(@Path("latlng") String latlng);

    @GET("{latlng},{time}?units=si&exclude=currently,flags")
    Single<WeatherForecast> getWeatherForecastForTime(@Path("latlng") String latlng,
                                                      @Path("time") long time);
}