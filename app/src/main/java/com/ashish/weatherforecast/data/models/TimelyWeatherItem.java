package com.ashish.weatherforecast.data.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.ashish.weatherforecast.utils.Constants;
import com.ashish.weatherforecast.utils.Utils;

@SuppressWarnings("ConstantConditions")
public class TimelyWeatherItem implements Parcelable {

    public static final Creator<TimelyWeatherItem> CREATOR = new Creator<TimelyWeatherItem>() {
        @Override
        public TimelyWeatherItem createFromParcel(Parcel in) {
            return new TimelyWeatherItem(in);
        }

        @Override
        public TimelyWeatherItem[] newArray(int size) {
            return new TimelyWeatherItem[size];
        }
    };

    private long time;
    private double maxTemp, minTemp;
    private String icon, title, subtitle, latLng;

    public TimelyWeatherItem(String icon, String title, String subtitle, double maxTemp,
                             double minTemp, long time, String latLng) {
        this.icon = icon;
        this.title = title;
        this.subtitle = subtitle;
        this.maxTemp = maxTemp;
        this.minTemp = minTemp;
        this.time = time;
        this.latLng = latLng;
    }

    protected TimelyWeatherItem(Parcel in) {
        time = in.readLong();
        maxTemp = in.readDouble();
        minTemp = in.readDouble();
        icon = in.readString();
        title = in.readString();
        subtitle = in.readString();
        latLng = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(time);
        dest.writeDouble(maxTemp);
        dest.writeDouble(minTemp);
        dest.writeString(icon);
        dest.writeString(title);
        dest.writeString(subtitle);
        dest.writeString(latLng);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public int getIcon() {
        return icon != null ? Constants.WEATHER_ICONS_MAP.get(icon) : 0;
    }

    public int getColor() {
        return icon != null ? Constants.CARD_COLOR_MAP.get(icon) : 0;
    }

    public String getTitle() {
        return title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public String getMaxTemp() {
        return maxTemp > 0 ? Utils.getRoundFormattedText("%d°", maxTemp) : null;
    }

    public String getMinTemp() {
        return minTemp > 0 ? Utils.getRoundFormattedText("%d°", minTemp) : null;
    }

    public long getTime() {
        return time;
    }

    public String getLatLng() {
        return latLng;
    }
}