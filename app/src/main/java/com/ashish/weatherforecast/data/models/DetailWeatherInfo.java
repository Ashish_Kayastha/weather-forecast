package com.ashish.weatherforecast.data.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.ashish.weatherforecast.utils.Utils;

public class DetailWeatherInfo implements Parcelable {

    public static final Creator<DetailWeatherInfo> CREATOR = new Creator<DetailWeatherInfo>() {
        @Override
        public DetailWeatherInfo createFromParcel(Parcel in) {
            return new DetailWeatherInfo(in);
        }

        @Override
        public DetailWeatherInfo[] newArray(int size) {
            return new DetailWeatherInfo[size];
        }
    };

    private double windGust;
    private int apparentTemperatureMinTime;
    private double temperatureMax;
    private String icon;
    private double precipIntensityMax;
    private int windBearing;
    private double ozone;
    private int temperatureMaxTime;
    private double apparentTemperatureMin;
    private int sunsetTime;
    private double temperatureLow;
    private String precipType;
    private double humidity;
    private double moonPhase;
    private double windSpeed;
    private int apparentTemperatureLowTime;
    private int sunriseTime;
    private double apparentTemperatureLow;
    private String summary;
    private double precipProbability;
    private double temperature;
    private double apparentTemperature;
    private int temperatureHighTime;
    private double visibility;
    private double precipIntensity;
    private double cloudCover;
    private double temperatureMin;
    private int apparentTemperatureHighTime;
    private double pressure;
    private double dewPoint;
    private int temperatureMinTime;
    private int uvIndexTime;
    private double apparentTemperatureMax;
    private double temperatureHigh;
    private int temperatureLowTime;
    private double apparentTemperatureHigh;
    private int time;
    private int precipIntensityMaxTime;
    private int windGustTime;
    private int uvIndex;
    private int apparentTemperatureMaxTime;

    protected DetailWeatherInfo(Parcel in) {
        windGust = in.readDouble();
        apparentTemperatureMinTime = in.readInt();
        temperatureMax = in.readDouble();
        icon = in.readString();
        precipIntensityMax = in.readDouble();
        windBearing = in.readInt();
        ozone = in.readDouble();
        temperatureMaxTime = in.readInt();
        apparentTemperatureMin = in.readDouble();
        sunsetTime = in.readInt();
        temperatureLow = in.readDouble();
        precipType = in.readString();
        humidity = in.readDouble();
        moonPhase = in.readDouble();
        windSpeed = in.readDouble();
        apparentTemperatureLowTime = in.readInt();
        sunriseTime = in.readInt();
        apparentTemperatureLow = in.readDouble();
        summary = in.readString();
        precipProbability = in.readDouble();
        temperature = in.readDouble();
        apparentTemperature = in.readDouble();
        temperatureHighTime = in.readInt();
        visibility = in.readDouble();
        precipIntensity = in.readDouble();
        cloudCover = in.readDouble();
        temperatureMin = in.readDouble();
        apparentTemperatureHighTime = in.readInt();
        pressure = in.readDouble();
        dewPoint = in.readDouble();
        temperatureMinTime = in.readInt();
        uvIndexTime = in.readInt();
        apparentTemperatureMax = in.readDouble();
        temperatureHigh = in.readDouble();
        temperatureLowTime = in.readInt();
        apparentTemperatureHigh = in.readDouble();
        time = in.readInt();
        precipIntensityMaxTime = in.readInt();
        windGustTime = in.readInt();
        uvIndex = in.readInt();
        apparentTemperatureMaxTime = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(windGust);
        dest.writeInt(apparentTemperatureMinTime);
        dest.writeDouble(temperatureMax);
        dest.writeString(icon);
        dest.writeDouble(precipIntensityMax);
        dest.writeInt(windBearing);
        dest.writeDouble(ozone);
        dest.writeInt(temperatureMaxTime);
        dest.writeDouble(apparentTemperatureMin);
        dest.writeInt(sunsetTime);
        dest.writeDouble(temperatureLow);
        dest.writeString(precipType);
        dest.writeDouble(humidity);
        dest.writeDouble(moonPhase);
        dest.writeDouble(windSpeed);
        dest.writeInt(apparentTemperatureLowTime);
        dest.writeInt(sunriseTime);
        dest.writeDouble(apparentTemperatureLow);
        dest.writeString(summary);
        dest.writeDouble(precipProbability);
        dest.writeDouble(temperature);
        dest.writeDouble(apparentTemperature);
        dest.writeInt(temperatureHighTime);
        dest.writeDouble(visibility);
        dest.writeDouble(precipIntensity);
        dest.writeDouble(cloudCover);
        dest.writeDouble(temperatureMin);
        dest.writeInt(apparentTemperatureHighTime);
        dest.writeDouble(pressure);
        dest.writeDouble(dewPoint);
        dest.writeInt(temperatureMinTime);
        dest.writeInt(uvIndexTime);
        dest.writeDouble(apparentTemperatureMax);
        dest.writeDouble(temperatureHigh);
        dest.writeInt(temperatureLowTime);
        dest.writeDouble(apparentTemperatureHigh);
        dest.writeInt(time);
        dest.writeInt(precipIntensityMaxTime);
        dest.writeInt(windGustTime);
        dest.writeInt(uvIndex);
        dest.writeInt(apparentTemperatureMaxTime);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public double getWindGust() {
        return windGust;
    }

    public int getApparentTemperatureMinTime() {
        return apparentTemperatureMinTime;
    }

    public double getTemperatureMax() {
        return temperatureMax;
    }

    public String getIcon() {
        return Utils.isNullOrEmpty(icon) ? "clear" : icon;
    }

    public double getPrecipIntensityMax() {
        return precipIntensityMax;
    }

    public int getWindBearing() {
        return windBearing;
    }

    public double getOzone() {
        return ozone;
    }

    public double getTemperature() {
        return temperature;
    }

    public double getApparentTemperature() {
        return apparentTemperature;
    }

    public int getTemperatureMaxTime() {
        return temperatureMaxTime;
    }

    public double getApparentTemperatureMin() {
        return apparentTemperatureMin;
    }

    public int getSunsetTime() {
        return sunsetTime;
    }

    public double getTemperatureLow() {
        return temperatureLow;
    }

    public String getPrecipType() {
        return Utils.isNullOrEmpty(precipType) ? "No Precipitation"
                : precipType.substring(0, 1).toUpperCase() + precipType.substring(1);
    }

    public double getHumidity() {
        return humidity;
    }

    public double getMoonPhase() {
        return moonPhase;
    }

    public double getWindSpeed() {
        return windSpeed;
    }

    public int getApparentTemperatureLowTime() {
        return apparentTemperatureLowTime;
    }

    public int getSunriseTime() {
        return sunriseTime;
    }

    public double getApparentTemperatureLow() {
        return apparentTemperatureLow;
    }

    public String getSummary() {
        return summary;
    }

    public double getPrecipProbability() {
        return precipProbability;
    }

    public int getTemperatureHighTime() {
        return temperatureHighTime;
    }

    public double getVisibility() {
        return visibility;
    }

    public double getPrecipIntensity() {
        return precipIntensity;
    }

    public double getCloudCover() {
        return cloudCover;
    }

    public double getTemperatureMin() {
        return temperatureMin;
    }

    public int getApparentTemperatureHighTime() {
        return apparentTemperatureHighTime;
    }

    public double getPressure() {
        return pressure;
    }

    public double getDewPoint() {
        return dewPoint;
    }

    public int getTemperatureMinTime() {
        return temperatureMinTime;
    }

    public int getUvIndexTime() {
        return uvIndexTime;
    }

    public double getApparentTemperatureMax() {
        return apparentTemperatureMax;
    }

    public double getTemperatureHigh() {
        return temperatureHigh;
    }

    public int getTemperatureLowTime() {
        return temperatureLowTime;
    }

    public double getApparentTemperatureHigh() {
        return apparentTemperatureHigh;
    }

    public int getTime() {
        return time;
    }

    public int getPrecipIntensityMaxTime() {
        return precipIntensityMaxTime;
    }

    public int getWindGustTime() {
        return windGustTime;
    }

    public int getUvIndex() {
        return uvIndex;
    }

    public int getApparentTemperatureMaxTime() {
        return apparentTemperatureMaxTime;
    }
}