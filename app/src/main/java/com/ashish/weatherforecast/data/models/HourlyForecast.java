package com.ashish.weatherforecast.data.models;

import com.ashish.weatherforecast.utils.Constants;
import com.ashish.weatherforecast.utils.Utils;

import java.util.Locale;

@SuppressWarnings("ConstantConditions")
public class HourlyForecast {

    private double temp, precipProb;
    private String icon, title, subtitle;

    public HourlyForecast(String icon, String title, String subtitle, double temp,
                          double precipProb) {
        this.icon = icon;
        this.title = title;
        this.subtitle = subtitle;
        this.temp = temp;
        this.precipProb = precipProb;
    }

    public int getIcon() {
        return icon != null ? Constants.WEATHER_ICONS_MAP.get(icon) : 0;
    }

    public int getColor() {
        return icon != null ? Constants.CARD_COLOR_MAP.get(icon) : 0;
    }

    public String getTitle() {
        return title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public String getTemperature() {
        return temp > 0 ? Utils.getRoundFormattedText("%d°", temp) : null;
    }

    public String getPrecipProb() {
        int precipitation = (int) (precipProb * 100);
        return precipProb > -1 ? String.format(Locale.getDefault(), "%d%% Rain",
                precipitation) : null;
    }

    public boolean hasHeader() {
        return temp == 0 && precipProb == -1;
    }
}