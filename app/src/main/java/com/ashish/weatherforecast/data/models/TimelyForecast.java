package com.ashish.weatherforecast.data.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class TimelyForecast implements Parcelable {

    public static final Creator<TimelyForecast> CREATOR = new Creator<TimelyForecast>() {
        @Override
        public TimelyForecast createFromParcel(Parcel in) {
            return new TimelyForecast(in);
        }

        @Override
        public TimelyForecast[] newArray(int size) {
            return new TimelyForecast[size];
        }
    };

    private String summary;
    private String icon;
    private List<DetailWeatherInfo> data;

    protected TimelyForecast(Parcel in) {
        summary = in.readString();
        icon = in.readString();
        data = in.createTypedArrayList(DetailWeatherInfo.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(summary);
        dest.writeString(icon);
        dest.writeTypedList(data);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public String getSummary() {
        return summary;
    }

    public List<DetailWeatherInfo> getData() {
        return data;
    }

    public String getIcon() {
        return icon;
    }
}