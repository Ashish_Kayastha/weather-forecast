package com.ashish.weatherforecast.data.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class WeatherForecast implements Parcelable {

    public static final Creator<WeatherForecast> CREATOR = new Creator<WeatherForecast>() {
        @Override
        public WeatherForecast createFromParcel(Parcel in) {
            return new WeatherForecast(in);
        }

        @Override
        public WeatherForecast[] newArray(int size) {
            return new WeatherForecast[size];
        }
    };

    private String locality;
    private double latitude;
    private double longitude;
    private String timezone;
    private TimelyForecast hourly;
    private TimelyForecast daily;
    private List<Alert> alerts;
    private DetailWeatherInfo currently;

    public WeatherForecast() {
    }

    protected WeatherForecast(Parcel in) {
        locality = in.readString();
        latitude = in.readDouble();
        longitude = in.readDouble();
        timezone = in.readString();
        hourly = in.readParcelable(TimelyForecast.class.getClassLoader());
        daily = in.readParcelable(TimelyForecast.class.getClassLoader());
        alerts = in.createTypedArrayList(Alert.CREATOR);
        currently = in.readParcelable(DetailWeatherInfo.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(locality);
        dest.writeDouble(latitude);
        dest.writeDouble(longitude);
        dest.writeString(timezone);
        dest.writeParcelable(hourly, flags);
        dest.writeParcelable(daily, flags);
        dest.writeTypedList(alerts);
        dest.writeParcelable(currently, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public String getTimezone() {
        return timezone;
    }

    public TimelyForecast getHourly() {
        return hourly;
    }

    public TimelyForecast getDaily() {
        return daily;
    }

    public List<Alert> getAlerts() {
        return alerts;
    }

    public DetailWeatherInfo getCurrently() {
        return currently;
    }

    public String getLocality() {
        return locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }
}