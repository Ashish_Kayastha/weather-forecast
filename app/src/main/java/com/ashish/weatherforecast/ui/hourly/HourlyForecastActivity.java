package com.ashish.weatherforecast.ui.hourly;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.ashish.weatherforecast.R;
import com.ashish.weatherforecast.data.models.DetailWeatherInfo;
import com.ashish.weatherforecast.data.models.HourlyForecast;
import com.ashish.weatherforecast.data.models.TimelyForecast;
import com.ashish.weatherforecast.ui.base.BaseActivity;
import com.ashish.weatherforecast.utils.DateTimeUtils;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;

public class HourlyForecastActivity extends BaseActivity {

    private static final String EXTRA_PLACE_NAME = "place_name";
    private static final String EXTRA_HOURLY_FORECAST = "hourly_forecast";

    @BindView(R.id.hourly_forecast_list) RecyclerView hourlyForecastList;

    private String placeName;
    private TimelyForecast hourlyForecast;

    public static Intent createIntent(Context context, TimelyForecast forecast, String placeName) {
        Intent intent = new Intent(context, HourlyForecastActivity.class);
        intent.putExtra(EXTRA_HOURLY_FORECAST, forecast);
        intent.putExtra(EXTRA_PLACE_NAME, placeName);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        showBackButton(placeName);

        hourlyForecastList.setLayoutManager(new LinearLayoutManager(this));
        hourlyForecastList.setHasFixedSize(true);

        showHourlyForecastList();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_hourly_forecast;
    }

    @Override
    protected void getIntentExtras() {
        placeName = getIntent().getStringExtra(EXTRA_PLACE_NAME);
        hourlyForecast = getIntent().getParcelableExtra(EXTRA_HOURLY_FORECAST);
    }

    private void showHourlyForecastList() {
        if (hourlyForecast != null) {
            List<HourlyForecast> hourlyItemList = new ArrayList<>();
            List<DetailWeatherInfo> hourlyData = hourlyForecast.getData();

            // Show next 24 hours weather forecast as first element
            hourlyItemList.add(new HourlyForecast(
                    hourlyForecast.getIcon(),
                    getString(R.string.next_24_hours),
                    hourlyForecast.getSummary(),
                    0,
                    -1
            ));

            // Show 24 hours data only
            for (int i = 0; i < 24; i++) {
                DetailWeatherInfo info = hourlyData.get(i);
                HourlyForecast item = new HourlyForecast(
                        info.getIcon(),
                        DateTimeUtils.getTimeAndDay(info.getTime()),
                        info.getSummary(),
                        info.getTemperature(),
                        info.getPrecipProbability()
                );

                hourlyItemList.add(item);
            }

            hourlyForecastList.setAdapter(new HourlyForecastAdapter(hourlyItemList));
        }
    }
}