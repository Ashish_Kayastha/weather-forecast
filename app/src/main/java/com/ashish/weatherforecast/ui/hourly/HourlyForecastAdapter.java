package com.ashish.weatherforecast.ui.hourly;

import android.content.Context;
import android.graphics.PorterDuff;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ashish.weatherforecast.R;
import com.ashish.weatherforecast.data.models.HourlyForecast;
import com.ashish.weatherforecast.utils.Constants;
import com.ashish.weatherforecast.utils.Utils;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class HourlyForecastAdapter extends
        RecyclerView.Adapter<HourlyForecastAdapter.HourlyForecastHolder> {

    private final List<HourlyForecast> hourlyItemList;

    public HourlyForecastAdapter(List<HourlyForecast> hourlyItemList) {
        this.hourlyItemList = hourlyItemList;
    }

    @NonNull
    @Override
    public HourlyForecastHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_hourly_forecast, parent, false);
        return new HourlyForecastHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HourlyForecastHolder holder, int position) {
        HourlyForecast hourlyForecast = hourlyItemList.get(position);

        holder.icon.setText(hourlyForecast.getIcon());

        Context context = holder.view.getContext();
        int topPosition = hourlyItemList.get(0).hasHeader() ? 1 : 0;

        if (position == topPosition) {
            holder.view.setBackground(context.getDrawable(R.drawable.bg_rounded_top));
        } else if (position == hourlyItemList.size() - 1) {
            holder.view.setBackground(context.getDrawable(R.drawable.bg_rounded_bottom));
        } else {
            holder.view.setBackgroundColor(Constants.PRIMARY_BLACK_COLOR);
        }

        int color = hourlyForecast.hasHeader() ? Constants.PRIMARY_BLACK_COLOR
                : hourlyForecast.getColor();
        holder.view.getBackground().setColorFilter(color, PorterDuff.Mode.SRC_ATOP);

        holder.titleText.setText(hourlyForecast.getTitle());
        holder.subtitleText.setText(hourlyForecast.getSubtitle());
        Utils.showOrHideText(holder.tempText, hourlyForecast.getTemperature());
        Utils.showOrHideText(holder.precipProbText, hourlyForecast.getPrecipProb());
    }

    @Override
    public int getItemCount() {
        return hourlyItemList.size();
    }

    class HourlyForecastHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.item_view) View view;
        @BindView(R.id.icon) TextView icon;
        @BindView(R.id.title_text) TextView titleText;
        @BindView(R.id.subtitle_text) TextView subtitleText;
        @BindView(R.id.temp_text) TextView tempText;
        @BindView(R.id.precip_prob_text) TextView precipProbText;

        HourlyForecastHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}