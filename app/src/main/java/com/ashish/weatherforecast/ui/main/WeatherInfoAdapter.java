package com.ashish.weatherforecast.ui.main;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ashish.weatherforecast.R;
import com.ashish.weatherforecast.data.models.DetailWeatherInfo;
import com.ashish.weatherforecast.data.models.WeatherForecast;
import com.ashish.weatherforecast.utils.Constants;
import com.ashish.weatherforecast.utils.DateTimeUtils;
import com.ashish.weatherforecast.utils.OnItemClickListener;
import com.ashish.weatherforecast.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class WeatherInfoAdapter extends RecyclerView.Adapter<WeatherInfoAdapter.WeatherInfoHolder> {

    private OnItemClickListener clickListener;
    private List<WeatherForecast> forecastList = new ArrayList<>();

    WeatherInfoAdapter(OnItemClickListener listener) {
        clickListener = listener;
    }

    @NonNull
    @Override
    public WeatherInfoHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_place_weather, parent, false);
        return new WeatherInfoHolder(view, clickListener);
    }

    @Override
    @SuppressLint("DefaultLocale")
    @SuppressWarnings("ConstantConditions")
    public void onBindViewHolder(@NonNull WeatherInfoHolder holder, int position) {
        WeatherForecast weatherForecast = forecastList.get(position);
        DetailWeatherInfo nowInfo = weatherForecast.getCurrently();

        holder.placeNameText.setText(weatherForecast.getLocality());
        holder.conditionText.setText(nowInfo.getSummary());
        holder.temperatureText.setText(Utils.getTemperature(nowInfo.getTemperature()));
        holder.timeText.setText(DateTimeUtils.convertTimeStamp(nowInfo.getTime()));

        int precipProb = (int) (nowInfo.getPrecipProbability() * 100);
        holder.precipProbText.setText(String.format("%d%%", precipProb));

        String icon = nowInfo.getIcon();
        holder.weatherIcon.setText(Constants.WEATHER_ICONS_MAP.get(icon));
        holder.weatherCard.setCardBackgroundColor(Constants.CARD_COLOR_MAP.get(icon));
    }

    @Override
    public int getItemCount() {
        return forecastList != null ? forecastList.size() : 0;
    }

    void addForecastList(@NonNull List<WeatherForecast> weatherForecastList, boolean refresh) {
        if (refresh) {
            int oldItemCount = getItemCount();
            forecastList.clear();
            notifyItemRangeRemoved(0, oldItemCount);
        }

        int oldPosition = getItemCount();
        forecastList.addAll(weatherForecastList);
        notifyItemRangeInserted(oldPosition, weatherForecastList.size());
    }

    WeatherForecast getForecast(int position) {
        return forecastList.get(position);
    }

    class WeatherInfoHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.time_text) TextView timeText;
        @BindView(R.id.weather_card) CardView weatherCard;
        @BindView(R.id.weather_icon) TextView weatherIcon;
        @BindView(R.id.condition_text) TextView conditionText;
        @BindView(R.id.place_name_text) TextView placeNameText;
        @BindView(R.id.precip_prob_text) TextView precipProbText;
        @BindView(R.id.temperature_text) TextView temperatureText;

        WeatherInfoHolder(@NonNull View itemView, OnItemClickListener listener) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(v -> listener.onItemClick(getAdapterPosition()));
        }
    }
}