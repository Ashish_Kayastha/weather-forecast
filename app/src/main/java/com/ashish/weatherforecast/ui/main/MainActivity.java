package com.ashish.weatherforecast.ui.main;

import android.app.Activity;
import android.content.Intent;
import android.location.Address;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.ashish.weatherforecast.R;
import com.ashish.weatherforecast.data.api.WeatherService;
import com.ashish.weatherforecast.data.models.WeatherForecast;
import com.ashish.weatherforecast.ui.base.BaseActivity;
import com.ashish.weatherforecast.ui.detail.DetailWeatherActivity;
import com.ashish.weatherforecast.ui.search.SearchActivity;
import com.ashish.weatherforecast.ui.widget.EmptyRecyclerView;
import com.ashish.weatherforecast.ui.widget.ItemOffsetDecoration;
import com.ashish.weatherforecast.ui.widget.MultiSwipeRefreshLayout;
import com.ashish.weatherforecast.utils.LocationManager;
import com.ashish.weatherforecast.utils.LocationUtils;
import com.ashish.weatherforecast.utils.OnItemClickListener;
import com.ashish.weatherforecast.utils.OnLocationUpdateListener;
import com.ashish.weatherforecast.utils.RetrofitHelper;
import com.ashish.weatherforecast.utils.Utils;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends BaseActivity implements OnLocationUpdateListener,
        SwipeRefreshLayout.OnRefreshListener, OnItemClickListener {

    public static final int REQUEST_SEARCH_LOCATION = 1001;
    private static final String TAG = "MainActivity";

    @BindView(R.id.empty_text) TextView emptyText;
    @BindView(R.id.weather_list) EmptyRecyclerView weatherList;
    @BindView(R.id.swipe_refresh) MultiSwipeRefreshLayout swipeRefreshLayout;

    private WeatherInfoAdapter adapter;
    private LocationManager locationManager;

    private WeatherService weatherService;
    private List<LatLng> latLngList = new ArrayList<>();
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        weatherService = RetrofitHelper.getWeatherService();

        locationManager = new LocationManager(this, this);

        swipeRefreshLayout.setSwipeableViews(weatherList, emptyText);
        swipeRefreshLayout.setOnRefreshListener(this);

        weatherList.setEmptyView(emptyText);
        weatherList.setLayoutManager(new LinearLayoutManager(this));
        weatherList.setHasFixedSize(true);
        weatherList.addItemDecoration(new ItemOffsetDecoration());
        adapter = new WeatherInfoAdapter(this);
        weatherList.setAdapter(adapter);

        locationManager.checkLocationUpdates();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_main;
    }

    private void getWeatherForecast(boolean refresh, LatLng... latlngList) {
        if (Utils.isOnline()) {
            if (latlngList.length != 0) {
                swipeRefreshLayout.setRefreshing(true);

                List<Single<WeatherForecast>> forecastObservableList =
                        getPlaceForecastObservableList(latlngList);

                if (!forecastObservableList.isEmpty()) {
                    compositeDisposable.add(Single
                            .zip(forecastObservableList, objects -> {
                                List<WeatherForecast> forecasts = new ArrayList<>(objects.length);
                                for (Object object : objects) {
                                    forecasts.add((WeatherForecast) object);
                                }
                                return forecasts;
                            })
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .doFinally(() -> swipeRefreshLayout.setRefreshing(false))
                            .subscribe((forecastList) -> showForecastList(forecastList, refresh),
                                    this::showErrorMessage));
                }
            }
        } else {
            Utils.showSnackbar(this, R.string.error_no_internet, android.R.string.ok, v -> {});
        }
    }

    private List<Single<WeatherForecast>> getPlaceForecastObservableList(LatLng[] latlngList) {
        List<Single<WeatherForecast>> observableList = new ArrayList<>();

        for (LatLng latlng : latlngList) {
            if (latlng != null) {
                observableList.add(weatherService
                        .getWeatherForecast(LocationUtils.getLatLng(latlng, ","))
                        .flatMap((Function<WeatherForecast, Single<WeatherForecast>>) weatherForecast ->
                                Single.fromCallable(() -> {
                                    Address address = LocationUtils.getAddress(latlng);
                                    if (address != null) {
                                        weatherForecast.setLocality(address.getLocality());
                                    }
                                    return weatherForecast;
                                }))
                        .subscribeOn(Schedulers.io())
                        .onErrorReturn(throwable -> new WeatherForecast()));
            }
        }

        return observableList;
    }

    private void showForecastList(List<WeatherForecast> weatherForecastList, boolean refresh) {
        if (weatherForecastList != null) {
            adapter.addForecastList(weatherForecastList, refresh);
        }
    }

    private void showErrorMessage(Throwable t) {
        Log.e(TAG, t.getMessage());
        Utils.showSnackbar(this, R.string.error_place_weather, android.R.string.ok, v -> {});
    }

    @Override
    public void onRefresh() {
        getWeatherForecast(true, latLngList.toArray(new LatLng[0]));
    }

    @Override
    public void onItemClick(int position) {
        WeatherForecast forecast = adapter.getForecast(position);
        startActivity(DetailWeatherActivity.createIntent(this, forecast));
    }

    @OnClick(R.id.add_place_fab)
    public void openSearchActivity() {
        Intent intent = new Intent(this, SearchActivity.class);
        startActivityForResult(intent, REQUEST_SEARCH_LOCATION);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        locationManager.checkActivityResult(requestCode, resultCode);

        if (requestCode == REQUEST_SEARCH_LOCATION && resultCode == Activity.RESULT_OK
                && data != null) {
            LatLng latLng = data.getParcelableExtra(SearchActivity.EXTRA_LAT_LNG);
            if (!latLngList.contains(latLng)) {
                latLngList.add(latLng);
                getWeatherForecast(false, latLng);
            }
        }
    }

    @Override
    public void onLocationUpdated(LatLng latLng) {
        if (!latLngList.contains(latLng)) {
            latLngList.add(latLng);
            getWeatherForecast(false, latLng);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        locationManager.onRequestPermissionsResult(requestCode);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        locationManager.stopLocationUpdates();
        if (!compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }
}