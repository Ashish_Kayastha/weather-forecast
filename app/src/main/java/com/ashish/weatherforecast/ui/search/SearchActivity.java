package com.ashish.weatherforecast.ui.search;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.widget.TextView;

import com.ashish.weatherforecast.R;
import com.ashish.weatherforecast.utils.AutoCompletePlaceHelper;
import com.ashish.weatherforecast.utils.LocationUtils;
import com.ashish.weatherforecast.utils.Utils;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.libraries.places.api.model.Place;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.content.res.AppCompatResources;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SearchActivity extends AppCompatActivity implements OnMapReadyCallback,
        GoogleMap.OnMapLongClickListener {

    public static final String EXTRA_LAT_LNG = "lat_lng";

    @BindView(R.id.search_text_view) TextView searchTextView;
    @BindView(R.id.add_location_text) TextView addLocationText;

    private Marker marker;
    private GoogleMap googleMap;
    private LatLng selectedLatLng;
    private Drawable checkDrawable;
    private AutoCompletePlaceHelper placeHelper;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        ButterKnife.bind(this);

        placeHelper = new AutoCompletePlaceHelper(this);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map_fragment);
        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        }

        checkDrawable = AppCompatResources.getDrawable(this, R.drawable.ic_check_green_24dp);
    }

    @Override
    public void onMapReady(GoogleMap map) {
        googleMap = map;
        googleMap.setOnMapLongClickListener(this);
    }

    @Override
    public void onMapLongClick(LatLng latLng) {
        placeMarker(latLng, null);
    }

    private void placeMarker(LatLng latLng, String placeName) {
        selectedLatLng = latLng;
        if (marker != null) marker.remove();

        marker = googleMap.addMarker(new MarkerOptions()
                .position(selectedLatLng)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));

        addLocationText.setCompoundDrawablesWithIntrinsicBounds(checkDrawable, null, null, null);
        if (!Utils.isNullOrEmpty(placeName)) {
            addLocationText.setText(getString(R.string.add_location_placeholder, placeName));
        } else {
            String ll = LocationUtils.getLatLng(selectedLatLng, ", ");
            addLocationText.setText(getString(R.string.add_location_placeholder, ll));
        }
    }

    @OnClick(R.id.back_icon)
    public void finishActivity() {
        finish();
    }

    @OnClick(R.id.add_location_text)
    public void addLocation() {
        if (selectedLatLng != null) {
            Intent returnIntent = new Intent();
            returnIntent.putExtra(EXTRA_LAT_LNG, selectedLatLng);
            setResult(Activity.RESULT_OK, returnIntent);
            finish();
        }
    }

    @OnClick(R.id.search_text_view)
    public void searchLocation() {
        placeHelper.startAutoCompletePlacesActivity();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Place place = placeHelper.getSelectedPlace(requestCode, resultCode, data);
        if (place != null) {
            String placeName = place.getName();
            searchTextView.setText(placeName);
            changeMapLocation(place.getLatLng(), placeName);
        }
    }

    private void changeMapLocation(LatLng latLng, String placeName) {
        CameraPosition camPosition = new CameraPosition.Builder()
                .target(latLng)
                .zoom(15)
                .build();

        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(camPosition));
        placeMarker(latLng, placeName);
    }
}