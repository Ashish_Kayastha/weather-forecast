package com.ashish.weatherforecast.ui.daily;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ashish.weatherforecast.R;
import com.ashish.weatherforecast.data.models.DetailWeatherInfo;
import com.ashish.weatherforecast.data.models.HourlyForecast;
import com.ashish.weatherforecast.data.models.TimelyWeatherItem;
import com.ashish.weatherforecast.ui.base.BaseDetailActivity;
import com.ashish.weatherforecast.ui.hourly.HourlyForecastAdapter;
import com.ashish.weatherforecast.utils.DateTimeUtils;
import com.ashish.weatherforecast.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class DailyForecastActivity extends BaseDetailActivity {

    private static final String TAG = "DailyForecastActivity";
    private static final String EXTRA_PLACE_NAME = "place_name";
    private static final String EXTRA_TIMELY_WEATHER = "timely_weather";

    @BindView(R.id.progress_bar) ProgressBar progressBar;
    @BindView(R.id.time_desc_text) TextView timeDescriptionText;
    @BindView(R.id.hourly_forecast_list) RecyclerView hourlyForecastList;

    private String placeName;
    private TimelyWeatherItem timelyWeatherItem;

    public static Intent createIntent(Context context, TimelyWeatherItem item, String placeName) {
        Intent intent = new Intent(context, DailyForecastActivity.class);
        intent.putExtra(EXTRA_PLACE_NAME, placeName);
        intent.putExtra(EXTRA_TIMELY_WEATHER, item);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        hourlyForecastList.setLayoutManager(new LinearLayoutManager(this));
        hourlyForecastList.setHasFixedSize(true);
        hourlyForecastList.setNestedScrollingEnabled(false);

        fetchDailyWeatherForecast();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_daily_forecast;
    }

    @Override
    protected void getIntentExtras() {
        placeName = getIntent().getStringExtra(EXTRA_PLACE_NAME);
        timelyWeatherItem = getIntent().getParcelableExtra(EXTRA_TIMELY_WEATHER);
    }

    @Override
    protected String getToolbarTitle() {
        return placeName;
    }

    private void fetchDailyWeatherForecast() {
        if (Utils.isOnline()) {
            if (timelyWeatherItem != null) {
                compositeDisposable.add(
                        weatherService.getWeatherForecastForTime(timelyWeatherItem.getLatLng(),
                                timelyWeatherItem.getTime())
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .doFinally(() -> {
                                    progressBar.setVisibility(View.GONE);
                                    weatherNowContainer.setVisibility(View.VISIBLE);
                                })
                                .subscribe(forecast -> {
                                    weatherForecast = forecast;
                                    showDetailWeatherInfo();
                                    showHourlyForecastList();
                                }, this::showErrorMessage)
                );
            }
        } else {
            Utils.showSnackbar(this, R.string.error_no_internet, android.R.string.ok, v -> {});
        }
    }

    private void showErrorMessage(Throwable t) {
        Log.e(TAG, "fetchDailyWeatherForecast: " + t.getMessage());
        Utils.showSnackbar(this, R.string.error_fetch_weather, android.R.string.ok, v -> {});
    }

    @Override
    protected void showDetailWeatherInfo() {
        super.showDetailWeatherInfo();
        timeDescriptionText.setText(DateTimeUtils.getDayOfMonth(getDetailInfo().getTime()));
    }

    @Override
    protected DetailWeatherInfo getDetailInfo() {
        return daily.getData().get(0);
    }

    @Override
    protected double getTemperature() {
        return hourly.getData().get(0).getTemperature();
    }

    private void showHourlyForecastList() {
        List<HourlyForecast> hourlyItemList = new ArrayList<>();
        List<DetailWeatherInfo> hourlyData = hourly.getData();

        long currentTime = System.currentTimeMillis() / 1000;

        for (DetailWeatherInfo hourlyInfo : hourlyData) {
            int time = hourlyInfo.getTime();
            // Show hourly data only if it is after the current time
            if (time > currentTime) {
                HourlyForecast item = new HourlyForecast(
                        hourlyInfo.getIcon(),
                        DateTimeUtils.convertTimeStamp(time),
                        hourlyInfo.getSummary(),
                        hourlyInfo.getTemperature(),
                        hourlyInfo.getPrecipProbability()
                );

                hourlyItemList.add(item);
            }
        }

        hourlyForecastList.setAdapter(new HourlyForecastAdapter(hourlyItemList));
    }
}