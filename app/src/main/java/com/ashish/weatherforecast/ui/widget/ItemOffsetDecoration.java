package com.ashish.weatherforecast.ui.widget;

import android.graphics.Rect;
import android.view.View;

import org.jetbrains.annotations.NotNull;

import androidx.recyclerview.widget.RecyclerView;

public final class ItemOffsetDecoration extends RecyclerView.ItemDecoration {

    private static final int ITEM_SPACING = 14;

    public ItemOffsetDecoration() {}

    @Override
    public void getItemOffsets(@NotNull Rect outRect, @NotNull View view,
                               RecyclerView parent, @NotNull RecyclerView.State state) {
        if (parent.getPaddingLeft() != ITEM_SPACING) {
            parent.setPadding(ITEM_SPACING, parent.getPaddingTop(), ITEM_SPACING, ITEM_SPACING);
            parent.setClipToPadding(false);
        }

        outRect.left = ITEM_SPACING;
        outRect.right = ITEM_SPACING;
        outRect.bottom = ITEM_SPACING;
    }
}