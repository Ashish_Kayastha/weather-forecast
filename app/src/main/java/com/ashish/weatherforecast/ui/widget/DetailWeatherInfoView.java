package com.ashish.weatherforecast.ui.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.Gravity;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ashish.weatherforecast.R;

import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailWeatherInfoView extends LinearLayout {

    @BindView(R.id.icon) TextView iconText;
    @BindView(R.id.title) TextView titleText;
    @BindView(R.id.subtitle) TextView subtitleText;

    private String icon, title, subtitle;

    public DetailWeatherInfoView(Context context) {
        this(context, null, 0);
    }

    public DetailWeatherInfoView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public DetailWeatherInfoView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        inflate(context, R.layout.view_detail_weather_info, this);

        if (attrs != null) {
            TypedArray ta = context
                    .obtainStyledAttributes(attrs, R.styleable.DetailWeatherInfoView);
            icon = ta.getString(R.styleable.DetailWeatherInfoView_dwiv_icon);
            title = ta.getString(R.styleable.DetailWeatherInfoView_dwiv_title);
            subtitle = ta.getString(R.styleable.DetailWeatherInfoView_dwiv_subtitle);
            ta.recycle();
        }
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this, this);
        setOrientation(VERTICAL);
        setGravity(Gravity.CENTER);

        setIcon(icon);
        setTitle(title);
        setSubtitle(subtitle);
    }

    public void setIcon(String icon) {
        iconText.setText(icon);
    }

    public void setTitle(String title) {
        titleText.setText(title);
    }

    public void setSubtitle(String subtitle) {
        subtitleText.setText(subtitle);
    }

    public TextView getIconTextView() {
        return iconText;
    }

    public TextView getTitleTextView() {
        return titleText;
    }

    public TextView getSubtitleTextView() {
        return subtitleText;
    }
}