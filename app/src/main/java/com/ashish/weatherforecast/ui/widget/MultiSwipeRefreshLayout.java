package com.ashish.weatherforecast.ui.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

/**
 * A descendant of {@link SwipeRefreshLayout} which supports multiple
 * child views triggering a refresh gesture. You set the views which can trigger the gesture via
 * {@link #setSwipeableViews(View...)}, providing it the child ids.
 */
public final class MultiSwipeRefreshLayout extends SwipeRefreshLayout {

    private View[] swipeableChildren;

    public MultiSwipeRefreshLayout(Context context) {
        super(context);
    }

    public MultiSwipeRefreshLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /**
     * Set the children views which can trigger a refresh by swiping down when they are visible.
     * These views need to be a descendant of this view.
     */
    public void setSwipeableViews(final View... views) {
        // Iterate through the ids and find the Views
        swipeableChildren = new View[views.length];
        swipeableChildren = views;
    }

    /**
     * This method controls when the swipe-to-refresh gesture is triggered. By returning false here
     * we are signifying that the view is in a state where a refresh gesture can start.
     * <p>
     * <p>As {@link SwipeRefreshLayout} only supports one direct child by
     * default, we need to manually iterate through our swipeable children to see if any are in a
     * state to trigger the gesture. If so we return false to start the gesture.
     */
    @Override
    public boolean canChildScrollUp() {
        if (swipeableChildren != null && swipeableChildren.length > 0) {
            // Iterate through the scrollable children and check if any of them can not scroll up
            for (View view : swipeableChildren) {
                if (view != null && view.isShown() && !view.canScrollVertically(-1)) {
                    // If the view is shown, and can not scroll upwards, return false and start
                    // the gesture.
                    return false;
                }
            }
        }
        return true;
    }
}