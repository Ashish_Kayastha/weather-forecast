package com.ashish.weatherforecast.ui.detail;

import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.os.Bundle;
import android.util.Log;

import com.ashish.weatherforecast.R;
import com.ashish.weatherforecast.data.models.DetailWeatherInfo;
import com.ashish.weatherforecast.data.models.TimelyWeatherItem;
import com.ashish.weatherforecast.data.models.WeatherForecast;
import com.ashish.weatherforecast.ui.base.BaseDetailActivity;
import com.ashish.weatherforecast.ui.daily.DailyForecastActivity;
import com.ashish.weatherforecast.ui.hourly.HourlyForecastActivity;
import com.ashish.weatherforecast.utils.DateTimeUtils;
import com.ashish.weatherforecast.utils.LocationUtils;
import com.ashish.weatherforecast.utils.OnItemClickListener;
import com.ashish.weatherforecast.utils.Utils;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import butterknife.BindView;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

public class DetailWeatherActivity extends BaseDetailActivity implements
        SwipeRefreshLayout.OnRefreshListener, OnItemClickListener {

    private static final String TAG = "DetailWeatherActivity";
    private static final String EXTRA_WEATHER_FORECAST = "weather_forecast";

    @BindView(R.id.timely_weather_list) RecyclerView timelyWeatherList;
    @BindView(R.id.swipe_refresh) SwipeRefreshLayout swipeRefreshLayout;

    private TimelyWeatherInfoAdapter weatherInfoAdapter;

    public static Intent createIntent(Context context, WeatherForecast forecast) {
        Intent intent = new Intent(context, DetailWeatherActivity.class);
        intent.putExtra(EXTRA_WEATHER_FORECAST, forecast);
        return intent;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        swipeRefreshLayout.setOnRefreshListener(this);

        timelyWeatherList.setLayoutManager(new LinearLayoutManager(this));
        timelyWeatherList.setHasFixedSize(true);
        timelyWeatherList.setNestedScrollingEnabled(false);
        weatherInfoAdapter = new TimelyWeatherInfoAdapter(this);
        timelyWeatherList.setAdapter(weatherInfoAdapter);

        showDetailWeatherInfo();
        showDetailWeatherList(false);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_detail;
    }

    @Override
    protected void getIntentExtras() {
        weatherForecast = getIntent().getParcelableExtra(EXTRA_WEATHER_FORECAST);
    }

    @Override
    protected String getToolbarTitle() {
        return weatherForecast.getLocality();
    }

    @Override
    protected DetailWeatherInfo getDetailInfo() {
        return weatherForecast.getCurrently();
    }

    private void showDetailWeatherList(boolean refresh) {
        List<TimelyWeatherItem> timelyWeatherItems = new ArrayList<>();

        DetailWeatherInfo hourlyInfo = hourly.getData().get(0);
        timelyWeatherItems.add(new TimelyWeatherItem(
                hourlyInfo.getIcon(),
                getString(R.string.next_hour),
                hourlyInfo.getSummary(),
                0,
                0,
                0,
                null
        ));

        timelyWeatherItems.add(new TimelyWeatherItem(
                hourly.getIcon(),
                getString(R.string.next_24_hours),
                hourly.getSummary(),
                0,
                0,
                0,
                null
        ));

        timelyWeatherItems.add(new TimelyWeatherItem(
                daily.getIcon(),
                getString(R.string.next_7_days),
                daily.getSummary(),
                0,
                0,
                0,
                null
        ));

        List<DetailWeatherInfo> dailyData = daily.getData();
        for (int i = 0; i < dailyData.size(); i++) {
            DetailWeatherInfo dailyInfo = dailyData.get(i);

            timelyWeatherItems.add(new TimelyWeatherItem(
                    dailyInfo.getIcon(),
                    DateTimeUtils.getDayOfMonth(dailyInfo.getTime()),
                    dailyInfo.getSummary(),
                    dailyInfo.getTemperatureMax(),
                    dailyInfo.getTemperatureMin(),
                    dailyInfo.getTime(),
                    weatherForecast.getLatitude() + "," + weatherForecast.getLongitude()
            ));
        }

        weatherInfoAdapter.updateWeatherItemList(timelyWeatherItems, refresh);
    }

    @Override
    public void onRefresh() {
        if (Utils.isOnline()) {
            double lat = weatherForecast.getLatitude();
            double lng = weatherForecast.getLongitude();

            compositeDisposable.add(weatherService.getWeatherForecast(lat + "," + lng)
                    .flatMap((Function<WeatherForecast, Single<WeatherForecast>>) weatherForecast ->
                            Single.fromCallable(() -> {
                                Address address = LocationUtils.getAddress(new LatLng(lat, lng));
                                if (address != null) {
                                    weatherForecast.setLocality(address.getLocality());
                                }
                                return weatherForecast;
                            }))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doFinally(() -> swipeRefreshLayout.setRefreshing(false))
                    .subscribe((forecast) -> {
                        weatherForecast = forecast;
                        showDetailWeatherInfo();
                        showDetailWeatherList(true);
                    }, (t) -> Log.e(TAG, t.getMessage())));
        } else {
            Utils.showSnackbar(this, R.string.error_no_internet, android.R.string.ok, v -> {});
        }
    }

    @Override
    public void onItemClick(int position) {
        String locality = weatherForecast.getLocality();
        if (position == 1) {
            startActivity(HourlyForecastActivity.createIntent(this, hourly, locality));
        } else {
            TimelyWeatherItem item = weatherInfoAdapter.getTimelyWeatherItem(position);
            startActivity(DailyForecastActivity.createIntent(this, item, locality));
        }
    }
}