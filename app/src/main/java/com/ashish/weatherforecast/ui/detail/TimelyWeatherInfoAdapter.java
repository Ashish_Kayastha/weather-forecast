package com.ashish.weatherforecast.ui.detail;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ashish.weatherforecast.R;
import com.ashish.weatherforecast.data.models.TimelyWeatherItem;
import com.ashish.weatherforecast.utils.OnItemClickListener;
import com.ashish.weatherforecast.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class TimelyWeatherInfoAdapter extends
        RecyclerView.Adapter<TimelyWeatherInfoAdapter.TimelyWeatherInfoHolder> {

    private final OnItemClickListener clickListener;
    private final List<TimelyWeatherItem> timelyWeatherItemList = new ArrayList<>();

    TimelyWeatherInfoAdapter(OnItemClickListener clickListener) {
        this.clickListener = clickListener;
    }

    @NonNull
    @Override
    public TimelyWeatherInfoHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_timely_weather, parent, false);
        return new TimelyWeatherInfoHolder(view, clickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull TimelyWeatherInfoHolder holder, int position) {
        TimelyWeatherItem weatherItem = timelyWeatherItemList.get(position);

        holder.icon.setText(weatherItem.getIcon());
        holder.view.setClickable(position != 0 && position != 2);

        Context context = holder.view.getContext();
        if (position == 1) {
            holder.view.setBackground(context.getDrawable(R.drawable.bg_rounded_top_bottom));
        } else if (position == 3) {
            holder.view.setBackground(context.getDrawable(R.drawable.bg_rounded_top));
        } else if (position == timelyWeatherItemList.size() - 1) {
            holder.view.setBackground(context.getDrawable(R.drawable.bg_rounded_bottom));
        } else {
            holder.view.setBackgroundColor(0xFF212121);
        }

        Drawable background = holder.view.getBackground();
        if (position == 0 || position == 2) {
            background.setColorFilter(0xFF212121, PorterDuff.Mode.SRC_ATOP);
        } else {
            background.setColorFilter(weatherItem.getColor(), PorterDuff.Mode.SRC_ATOP);
        }

        holder.titleText.setText(weatherItem.getTitle());
        holder.subtitleText.setText(weatherItem.getSubtitle());
        Utils.showOrHideText(holder.highTempText, weatherItem.getMaxTemp());
        Utils.showOrHideText(holder.lowTempText, weatherItem.getMinTemp());
    }

    void updateWeatherItemList(List<TimelyWeatherItem> weatherItems, boolean refresh) {
        if (refresh) {
            int oldItemCount = getItemCount();
            timelyWeatherItemList.clear();
            notifyItemRangeRemoved(0, oldItemCount);
        }

        int oldPosition = getItemCount();
        timelyWeatherItemList.addAll(weatherItems);
        notifyItemRangeInserted(oldPosition, weatherItems.size());
    }

    @Override
    public int getItemCount() {
        return timelyWeatherItemList.size();
    }

    TimelyWeatherItem getTimelyWeatherItem(int position) {
        return timelyWeatherItemList.get(position);
    }

    class TimelyWeatherInfoHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.item_view) View view;
        @BindView(R.id.icon) TextView icon;
        @BindView(R.id.title_text) TextView titleText;
        @BindView(R.id.subtitle_text) TextView subtitleText;
        @BindView(R.id.high_temp_text) TextView highTempText;
        @BindView(R.id.low_temp_text) TextView lowTempText;

        TimelyWeatherInfoHolder(@NonNull View itemView, OnItemClickListener listener) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(v -> listener.onItemClick(getAdapterPosition()));
        }
    }
}