package com.ashish.weatherforecast.ui.base;

import android.graphics.PorterDuff;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.ashish.weatherforecast.R;
import com.ashish.weatherforecast.data.api.WeatherService;
import com.ashish.weatherforecast.data.models.DetailWeatherInfo;
import com.ashish.weatherforecast.data.models.TimelyForecast;
import com.ashish.weatherforecast.data.models.WeatherForecast;
import com.ashish.weatherforecast.ui.widget.DetailWeatherInfoView;
import com.ashish.weatherforecast.utils.Constants;
import com.ashish.weatherforecast.utils.DateTimeUtils;
import com.ashish.weatherforecast.utils.RetrofitHelper;
import com.ashish.weatherforecast.utils.Utils;

import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import androidx.annotation.Nullable;
import butterknife.BindView;
import io.reactivex.disposables.CompositeDisposable;

public abstract class BaseDetailActivity extends BaseActivity {

    @BindView(R.id.detail_weather_icon) protected TextView weatherIcon;
    @BindView(R.id.weather_now_container) protected View weatherNowContainer;
    @BindView(R.id.detail_temperature_text) protected TextView temperatureText;
    @BindView(R.id.detail_weather_condition_text) protected TextView conditionText;

    @BindView(R.id.sun_time_view) protected DetailWeatherInfoView sunTimeView;
    @BindView(R.id.humidity_view) protected DetailWeatherInfoView humidityView;
    @BindView(R.id.pressure_view) protected DetailWeatherInfoView pressureView;
    @BindView(R.id.feels_like_view) protected DetailWeatherInfoView feelsLikeView;
    @BindView(R.id.precip_prob_view) protected DetailWeatherInfoView precipProbView;

    @BindView(R.id.wind_view) protected DetailWeatherInfoView windView;
    @BindView(R.id.dew_point_view) protected DetailWeatherInfoView dewPointView;
    @BindView(R.id.moon_phase_view) protected DetailWeatherInfoView moonPhaseView;
    @BindView(R.id.visibility_view) protected DetailWeatherInfoView visibilityView;
    @BindView(R.id.temperature_view) protected DetailWeatherInfoView temperatureView;

    protected TimelyForecast hourly, daily;
    protected WeatherForecast weatherForecast;

    protected WeatherService weatherService;
    protected CompositeDisposable compositeDisposable = new CompositeDisposable();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        showBackButton(getToolbarTitle());
        weatherService = RetrofitHelper.getWeatherService();
    }

    protected abstract String getToolbarTitle();

    @SuppressWarnings("ConstantConditions")
    protected void showDetailWeatherInfo() {
        if (weatherForecast != null) {
            hourly = weatherForecast.getHourly();
            daily = weatherForecast.getDaily();

            DetailWeatherInfo detailInfo = getDetailInfo();

            conditionText.setText(detailInfo.getSummary());

            String icon = detailInfo.getIcon();
            weatherIcon.setText(Constants.WEATHER_ICONS_MAP.get(icon));
            weatherNowContainer.getBackground().setColorFilter(Constants.CARD_COLOR_MAP.get(icon),
                    PorterDuff.Mode.SRC_ATOP);

            double temperature = getTemperature();
            temperatureText.setText(Utils.getTemperature(temperature));

            double tempInF = (temperature * 9 / 5) + 32;
            temperatureView.setTitle(Utils.getTemperature(tempInF));

            int precipProb = (int) (detailInfo.getPrecipProbability() * 100);
            precipProbView.setTitle(String.format(Locale.getDefault(), "%d%%", precipProb));
            precipProbView.setSubtitle(Utils.formatTo3DecimalPlaces(
                    detailInfo.getPrecipIntensity()));

            pressureView.setTitle(Utils.getRoundFormattedText("%d hPa", detailInfo.getPressure()));
            feelsLikeView.setTitle(Utils.getTemperature(
                    hourly.getData().get(0).getApparentTemperature()));

            int humidity = (int) (detailInfo.getHumidity() * 100);
            humidityView.setTitle(Utils.getRoundFormattedText("%d%%", humidity));

            showSunTime();
            showMoonPhase(daily.getData().get(0));

            int windBearing = detailInfo.getWindBearing();
            windView.getIconTextView().setAnimation(Utils.rotateWindIcon(windBearing));
            windView.setTitle(Utils.getRoundFormattedText("%d m/s", detailInfo.getWindSpeed()));
            windView.setSubtitle(Utils.getWindDirection(windBearing));

            dewPointView.setTitle(Utils.getTemperature(detailInfo.getDewPoint()));
            visibilityView.setTitle(Utils.getRoundFormattedText("%d km",
                    detailInfo.getVisibility()));
        }
    }

    protected abstract DetailWeatherInfo getDetailInfo();

    protected double getTemperature() {
        return getDetailInfo().getTemperature();
    }

    private void showSunTime() {
        Calendar cal = Calendar.getInstance();
        int hourOfDay = cal.get(Calendar.HOUR_OF_DAY);
        TimeZone timeZone = TimeZone.getTimeZone(weatherForecast.getTimezone());

        DetailWeatherInfo dailyInfo;
        List<DetailWeatherInfo> dailyData = daily.getData();
        if (dailyData.size() == 1 || hourOfDay < 20) {
            dailyInfo = dailyData.get(0);
        } else {
            dailyInfo = dailyData.get(1);
        }

        long sunriseTime = dailyInfo.getSunriseTime();
        long sunsetTime = dailyInfo.getSunsetTime();

        // Show sunrise time if the current time is before 9AM or after 8PM
        if (hourOfDay <= 9 || hourOfDay >= 20) {
            sunTimeView.setIcon(getString(R.string.wi_sunrise));
            sunTimeView.setSubtitle(getString(R.string.sunrise_txt));
            sunTimeView.setTitle(DateTimeUtils.convertTimeStamp(sunriseTime, timeZone));
        } else {
            sunTimeView.setIcon(getString(R.string.wi_sunset));
            sunTimeView.setSubtitle(getString(R.string.sunset_txt));
            sunTimeView.setTitle(DateTimeUtils.convertTimeStamp(sunsetTime, timeZone));
        }
    }

    private void showMoonPhase(DetailWeatherInfo dailyInfo) {
        double moonPhase = dailyInfo.getMoonPhase();
        if (moonPhase == 0) {
            setMoonPhase(R.string.wi_moon_new, "New", "Moon");
        } else if (moonPhase < 0.25) {
            setMoonPhase(R.string.wi_moon_waxing_crescent_1, "Waxing", "Crescent");
        } else if (moonPhase == 0.25) {
            setMoonPhase(R.string.wi_moon_first_quarter, "First", "Quarter");
        } else if (moonPhase < 0.5) {
            setMoonPhase(R.string.wi_moon_waxing_gibbous_1, "Waxing", "Gibbous");
        } else if (moonPhase == 0.5) {
            setMoonPhase(R.string.wi_moon_full, "Full", "Moon");
        } else if (moonPhase < 0.75) {
            setMoonPhase(R.string.wi_moon_waning_gibbous_1, "Waning", "Gibbous");
        } else if (moonPhase == 0.75) {
            setMoonPhase(R.string.wi_moon_third_quarter, "Last", "Quarter");
        } else if (moonPhase > 0.75) {
            setMoonPhase(R.string.wi_moon_waning_crescent_1, "Waning", "Crescent");
        }
    }

    private void setMoonPhase(int iconId, String title, String subtitle) {
        moonPhaseView.setIcon(getString(iconId));
        moonPhaseView.setTitle(title);
        moonPhaseView.setSubtitle(subtitle);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (!compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }
}